module ToBeSerialized
  alias :to_json_old :to_json

  def escape_special_chars(string)
    # escaping special chars
    pattern = /(\'|\"|\&|\/|\\|\@)/
    string.gsub(pattern){ |match|"\\" + match }
  end

  def ToBeSerialized.included(base)
    # when we include module we hook our to_json method
    base.send(:define_method, "to_json") do
      arr = @variable_with_list_of_variables_that_we_will_serialize
      
      # running old to_json function
      json = self.to_json_old

      # parsing old json response into hash and deleting elements that are not defined.
      # this is stupid thing to do i did it only because my task requires to call old to_json
      # in real app i would just redefine to_json
      new_hash = Hash[*json.sub!(/,\"variable_with_list_of_variables_that_we_will_serialize.*?\".*?\".*?\"/, '').gsub!(/\"(.*?)\"/, '\1')[1..-2].split(/[,|:]/)].delete_if do |key, val|
        !arr.nil? && !arr.include?(key.to_sym)
      end

      # parsing back to json also we escape special chars
      arr = []
      new_hash.map do |key, val|
        val = escape_special_chars(val)
        arr << '"' + key.to_s + '":"' + val.to_s + '"' unless val.nil?
      end

      "{#{arr * ","}}" unless new_hash.empty?
    end
  end

  def to_be_serialized(args = nil)
    # we use this long, overcomlicated variable name in order to not redefine some instance variable by accident
    self.instance_variable_set(:@variable_with_list_of_variables_that_we_will_serialize, args)
  end

  def method_missing(meth, *args, &block)
    # checking if it is our dynamic method
    if meth.to_s =~ /^to_json_for_(.*)$/
      run_find_for($1)
    else
      super
    end
  end

  def run_find_for(meth)
    # dynamic method
    args = meth.split('_and_')
    args.map!(&:to_sym)

    # running our methods picking variable names from method name
    self.to_be_serialized(args)
    self.to_json
  end
end
