require "to_json/version"

class Object
  def to_json
    # get all instance variables and their values
    variables = Hash[self.instance_variables.map { |name| [name, self.instance_variable_get(name)] }]

    arr = []
    # adding brackets and cuting '@' from variable names
    variables.each_pair { |key, val| arr << '"' + key.to_s[1..-1] + '":"' + val.to_s + '"' }

    # formatting as json string
    "{#{arr * ","}}" unless variables.empty?
  end

  private 
end
