require 'spec_helper'

describe ToJson do
  it "should return class formated as String in json" do
    class AnyClass
      def initialize(asd, dsa)
        @asd, @dsa = asd, dsa
      end
    end

    obj = AnyClass.new(123, 'abc')
    obj.to_json.should == '{"asd":"123","dsa":"abc"}'
  end

  it "should answer with nil to object without instance variables" do
    class OtherClass
      CONST = 123
      @@some_var = "str"
    end

    other_obj = OtherClass.new
    other_obj.to_json.should == nil
  end
end
