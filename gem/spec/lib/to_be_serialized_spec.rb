require 'spec_helper'
require "to_json/to_be_serialized"

describe ToBeSerialized do
  it "should have mixin ToBeSerialized and successfully update to_json method" do
    class AnyClass
      include ToBeSerialized

      def initialize(asd, dsa, bcc)
        @asd, @dsa, @bcc = asd, dsa, bcc
      end
    end

    obj = AnyClass.new(123, 'ab&c', 333)
    obj.to_be_serialized([:asd, :dsa, :bbb])
    obj.to_json.should == '{"asd":"123","dsa":"ab\&c"}'
  end

  it "should escape this characters: <'>, <&>, </>, <\\>, <@>" do
    class AnyClass
      include ToBeSerialized

      def initialize(asd, dsa, bcc)
        @asd, @dsa, @bcc = asd, dsa, bcc
      end
    end

    obj = AnyClass.new(123, '\'&/\\@', 333)
    obj.to_be_serialized([:asd, :dsa, :bbb])
    obj.to_json.should == '{"asd":"123","dsa":"\\\'\\&\\/\\\\\\@"}' # equal {"asd":"123","dsa":"\'\&\/\\\@"}
  end

  context "dynamical method to_json_by_name" do
    it "should call hooked method to_json" do
      class AnyClass
        include ToBeSerialized

        def initialize(asd, dsa, bcc)
          @asd, @dsa, @bcc = asd, dsa, bcc
        end
      end

      obj = AnyClass.new(123, 'a&@bc', 333)
      obj.to_json_for_asd_and_dsa_and_bcc.should == '{"asd":"123","dsa":"a\&\@bc","bcc":"333"}'
    end

    it "shouldn't be affected by to_be_serialized method call" do
      class AnyClass
        include ToBeSerialized

        def initialize(asd, dsa, bcc)
          @asd, @dsa, @bcc = asd, dsa, bcc
        end
      end

      obj = AnyClass.new(123, 'abc', 333)
      obj.to_be_serialized([:asd, :dsa, :bbb])
      obj.to_json_for_asd_and_bcc.should == '{"asd":"123","bcc":"333"}'
    end
  end
end
