# ToJson

Adds to_json method to all classes

## Installation

Add this line to your application's Gemfile:

    gem 'to_json'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install to_json

## Usage

You can call 'to_json' method from any class in your application, it will return a String of all methods and properties of current Class.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
