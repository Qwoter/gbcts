class CreatePersonalBlogs < ActiveRecord::Migration
  def change
    create_table :personal_blogs do |t|
      t.string :name
      t.string :description
      t.references :user

      t.timestamps
    end
  end
end
