class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :name
      t.string :body
      t.references :personal_blog

      t.timestamps
    end
  end
end
