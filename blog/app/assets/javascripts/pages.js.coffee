# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$("form#user-login-form").bind("ajax:beforeSend", (evt, xhr, settings) ->
).bind("ajax:success", (evt, data, status, xhr) ->
  if data.success is false
    $("form#user-login-form .alert-error").text(data.errors).show()
  console.log evt + data.errors + status + xhr
  $("#comments").append xhr.responseText
).bind("ajax:complete", (evt, xhr, status) ->
).bind "ajax:error", (evt, xhr, status, error) ->
  alert "error"
  try
    errors = $.parseJSON(xhr.responseText)
  catch err
    errors = message: "Please reload the page and try again"
  errorText = "There were errors with the submission: \n<ul>"
  for error of errors
    errorText += "<li>" + error + ": " + errors[error] + "</li> "
  errorText += "</ul>"
  
  # Insert error list into form
  $form.find("div.validation-error").html errorText
