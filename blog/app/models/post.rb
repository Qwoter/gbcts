class Post < ActiveRecord::Base
  attr_accessible :body, :name

  belongs_to :user
  belongs_to :personal_blog
end
