class BlogController < ApplicationController

  def edit
    @blog = Test.find(current_user.blog)
  end

  def create
    @blog = Test.new(params[:test])

    respond_to do |format|
      if @blog.save
        format.html { redirect_to @blog, notice: 'Test was successfully created.' }
        format.json { render json: @blog, status: :created, location: @blog }
      else
        format.html { render action: "new" }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @blog = Test.find(params[:id])

    respond_to do |format|
      if @blog.update_attributes(params[:test])
        format.html { redirect_to @blog, notice: 'Test was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @blog = Test.find(params[:id])
    @blog.destroy

    respond_to do |format|
      format.html { redirect_to tests_url }
      format.json { head :no_content }
    end
  end
end