$(document).ready ->
  $("div").disableSelection()
  $("#display").data("data", { method: "", stack: "" })
  $("#display").data("cosmetic", { to_clear: false })

$ ->
  clear_display = (a = 0) ->
    $("#display").text a

  backspace = ->
    temp = $("#display").text().trim()
    temp = temp.substr(0, temp.length - 1)

    if temp.length == 0
      temp = 0
    $("#display").text(temp)

  add_display_num = (obj) ->
    if $("#display").data("cosmetic").to_clear
      clear_display()

    if $("#display").length == 1 && $("#display").text().trim() == '0' && obj.text() != "."
      $("#display").text(obj.text())
    else if $("#display").text().length < 10
      $("#display").text($("#display").text().trim() + obj.text())

    $("#display").data("cosmetic", clear: false )

  plus_minus = ->
    ab = $("#display").text().trim()

    if ab.substr(0, 1) == '-'
      $("#display").text -ab
    else if $("#display").text().trim().length < 10
      $("#display").text -ab

  stack = (m) ->
    if $("#display").data("data").method == "" || $("#display").data("data").number != $("#display").text().trim()
      switch m
        when '/'
          s = "#{$("#display").data("data").stack}#{$("#display").text().trim()}.to_f #{m} "
        when 'tg(x)'
          s = "#{$("#display").data("data").stack}#{$("#display").text().trim()} #{m} "
        else
          s = "#{$("#display").data("data").stack}#{$("#display").text().trim()} #{m} "

      $("#display").data("data", { method: m, stack: s, number: $("#display").text().trim() })
      to_clear()
      console.log $("#display").data("data")
    else if $("#display").data("data").method == m
      switch m
        when '+'
          s = "#{$("#display").text().trim()}#{m}#{$("#display").text().trim()}"
          $("#display").data("data", { method: "", stack: s })
          console.log $("#display").data("data")
        else

      ajax()

  ajax = ->
    $.ajax
      url:'/calc/calculate'
      dataType:'html'
      data: { stack: $("#display").data("data").stack }
      type: 'post'

      success: (data) ->
        clear_stack()
        clear_display(data)
        console.log "success #{data}"
      error: (data) ->
        clear_stack()
        clear_display(data)
        console.log data

  clear_stack = ->
    $("#display").data("data", { method: "", stack: "" })
    $("#display").data("cosmetic", { to_clear: false })

  to_clear = ->
    $("#display").data("cosmetic", to_clear: true )

  plus = ->
    stack("+")
    
  minus = ->
    stack("-")

  multiply = ->
    stack("*")

  divide = ->
    stack("/")

  calculate = ->
    if $("#display").data("cosmetic").to_clear
      s = "#{$("#display").data("data").stack}0"
    else
      s = "#{$("#display").data("data").stack}#{$("#display").text().trim()}"

    $("#display").data("data", stack: s)

    ajax()

  $("#clear_btn").click ->
    clear_stack()
    clear_display()

  $("#backspace").click ->
    backspace()

  $(".number_btn").click ->
    add_display_num($(this))

  $("#plus_minus").click ->
    plus_minus()

  $("#plus").click ->
    plus()

  $("#minus").click ->
    minus()

  $("#multiply").click ->
    multiply()

  $("#divide").click ->
    divide()

  $("#root2").click ->
    root2()

  $("#root3").click ->
    root3()

  $("#root4").click ->
    root4()

  $("#calculate").click ->
    calculate()
