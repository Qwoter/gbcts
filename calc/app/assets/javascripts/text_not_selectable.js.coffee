$.fn.extend disableSelection: ->
  @each ->
    unless typeof @onselectstart is "undefined"
      @onselectstart = ->
        false
    else unless typeof @style.MozUserSelect is "undefined"
      @style.MozUserSelect = "none"
    else
      @onmousedown = ->
        false
