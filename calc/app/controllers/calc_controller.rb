class CalcController < ApplicationController
  def calculate
    calc = Calculator.new(params[:stack])

    calc.check ? result = calc.result : result = calc.error

    respond_to do |format|
      format.json { render :json => result.to_json }
    end
  end
end
